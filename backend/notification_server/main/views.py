
from rest_framework import generics
from rest_framework.response import Response

from .models import Client, Newsletter, Message
from .serializers import ClientShowSerializer, ClientSerializer
from .serializers import NewsletterSerializer, NewsletterListSerializer
from .serializers import MessageListSerializer

from .services.views.services import delete_connect_task


class ClientListView(generics.ListAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientShowSerializer


class ClientCreateView(generics.CreateAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class ClientUpdateView(generics.UpdateAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    lookup_field = 'id'


class ClientDeleteView(generics.DestroyAPIView):
    queryset = Client.objects.all()
    lookup_field = 'id'
    

class NewsletterListView(generics.ListAPIView):
    queryset = Newsletter.objects.all()
    serializer_class = NewsletterListSerializer


class NewsletterCreateView(generics.CreateAPIView):
    queryset = Newsletter.objects.all()
    serializer_class = NewsletterSerializer


class NewsletterUpdateView(generics.UpdateAPIView):
    queryset = Newsletter.objects.all()
    serializer_class = NewsletterSerializer
    lookup_field = 'id'


class NewsletterDeleteView(generics.DestroyAPIView):
    queryset = Newsletter
    lookup_field = 'id'

    def perform_destroy(self, instance):
        delete_connect_task(instance)
        instance.delete()


class MessageListView(generics.ListAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageListSerializer
    lookup_field = 'id'

    def list(self, request, *args, **kwargs):
        newsletter_id = self.kwargs.get(self.lookup_field)
        queryset = self.filter_queryset(self.get_queryset().filter(newsletter=newsletter_id))

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


